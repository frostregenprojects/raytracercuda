
#ifndef MYMATRIX_H
#define MYMATRIX_H

#include "cuda_runtime.h"

typedef struct
{
    float4 R1;
		float4 R2;
		float4 R3;
		float4 R4;
} Matrix;

inline __device__ __host__ Matrix Identity()
{
	Matrix newMatrix;
	newMatrix.R1 = make_float4(1.0f, 0.0f, 0.0f, 0.0f);
	newMatrix.R2 = make_float4(0.0f, 1.0f, 0.0f, 0.0f);
	newMatrix.R3 = make_float4(0.0f, 0.0f, 1.0f, 0.0f);
	newMatrix.R4 = make_float4(0.0f, 0.0f, 0.0f, 1.0f);
	return newMatrix;
}

inline __device__ __host__ Matrix CreateLookAt(float3 cameraPosition,
	float3 cameraTarget, float3 cameraUpVector)
{
	float3 dir = normalize(cameraPosition - cameraTarget);
	float3 up = normalize(cross(cameraUpVector, dir));
	// Switch up around if cameraUpVector is the same as dir!
	if (dot(up, up) == 0)
	{
		up = make_float3(0.0f, 1.0f, 0.0f);
	}
	float3 right = cross(dir, up);
	Matrix result;
	result.R1 = make_float4(up.x, right.x, dir.x, 0.0f);
	result.R2 = make_float4(up.y, right.y, dir.y, 0.0f);
	result.R3 = make_float4(up.z, right.z, dir.z, 0.0f);
	result.R4 = make_float4(
		-dot(up, cameraPosition),
		-dot(right, cameraPosition),
		-dot(dir, cameraPosition),
		1.0f);
	return result;
}

inline __device__ __host__ float3 Transform(float3 position, Matrix matrix)
{
	return make_float3(
	// X
	(position.x * matrix.R1.x) + (position.y * matrix.R2.x) +
	(position.z * matrix.R3.x) + matrix.R4.x,
	// Y
	(position.x * matrix.R1.y) + (position.y * matrix.R2.y) +
	(position.z * matrix.R3.y) + matrix.R4.y,
	// Z
	(position.x * matrix.R1.z) + (position.y * matrix.R2.z) +
	(position.z * matrix.R3.z) + matrix.R4.z);
} // Transform(position, matrix)

inline __device__ __host__ Matrix CreatePerspective(float fieldOfView,
	float aspectRatio, float nearPlaneDistance, float farPlaneDistance)
{
	float m22 = 1.0f / ((float)tan(fieldOfView * 0.5f));
	Matrix result;
	result.R1 = make_float4(m22 / aspectRatio, 0.0f, 0.0f, 0.0f);
	result.R2 = make_float4(0.0f, m22, 0.0f, 0.0f);
	result.R3 = make_float4(0.0f, 0.0f, farPlaneDistance / (nearPlaneDistance - farPlaneDistance), -1.0f);
	result.R4 = make_float4(0.0f, 0.0f, (nearPlaneDistance * farPlaneDistance) / (nearPlaneDistance - farPlaneDistance), 0.0f);
	return result;
}

inline __device__ __host__ Matrix MMultiply(Matrix matrix1, Matrix matrix2)
{
	Matrix result;
	result.R1 = make_float4(
		(matrix1.R1.x * matrix2.R1.x) + (matrix1.R1.y * matrix2.R2.x) + (matrix1.R1.z * matrix2.R3.x) + (matrix1.R1.w * matrix2.R4.x),
		(matrix1.R1.x * matrix2.R1.y) + (matrix1.R1.y * matrix2.R2.y) + (matrix1.R1.z * matrix2.R3.y) + (matrix1.R1.w * matrix2.R4.y),
		(matrix1.R1.x * matrix2.R1.z) + (matrix1.R1.y * matrix2.R2.z) + (matrix1.R1.z * matrix2.R3.z) + (matrix1.R1.w * matrix2.R4.z),
		(matrix1.R1.x * matrix2.R1.w) + (matrix1.R1.y * matrix2.R2.w) + (matrix1.R1.z * matrix2.R3.w) + (matrix1.R1.w * matrix2.R4.w));

	result.R2 = make_float4(
		(matrix1.R2.x * matrix2.R1.x) + (matrix1.R2.y * matrix2.R2.x) + (matrix1.R2.z * matrix2.R3.x) + (matrix1.R2.w * matrix2.R4.x),
		(matrix1.R2.x * matrix2.R1.y) + (matrix1.R2.y * matrix2.R2.y) + (matrix1.R2.z * matrix2.R3.y) + (matrix1.R2.w * matrix2.R4.y),
		(matrix1.R2.x * matrix2.R1.z) + (matrix1.R2.y * matrix2.R2.z) + (matrix1.R2.z * matrix2.R3.z) + (matrix1.R2.w * matrix2.R4.z),
		(matrix1.R2.x * matrix2.R1.w) + (matrix1.R2.y * matrix2.R2.w) + (matrix1.R2.z * matrix2.R3.w) + (matrix1.R2.w * matrix2.R4.w));

	result.R3 = make_float4(
		(matrix1.R3.x * matrix2.R1.x) + (matrix1.R3.y * matrix2.R2.x) + (matrix1.R3.z * matrix2.R3.x) + (matrix1.R3.w * matrix2.R4.x),
		(matrix1.R3.x * matrix2.R1.y) + (matrix1.R3.y * matrix2.R2.y) + (matrix1.R3.z * matrix2.R3.y) + (matrix1.R3.w * matrix2.R4.y),
		(matrix1.R3.x * matrix2.R1.z) + (matrix1.R3.y * matrix2.R2.z) + (matrix1.R3.z * matrix2.R3.z) + (matrix1.R3.w * matrix2.R4.z),
		(matrix1.R3.x * matrix2.R1.w) + (matrix1.R3.y * matrix2.R2.w) + (matrix1.R3.z * matrix2.R3.w) + (matrix1.R3.w * matrix2.R4.w));

	result.R4 = make_float4(
		(matrix1.R4.x * matrix2.R1.x) + (matrix1.R4.y * matrix2.R2.x) + (matrix1.R4.z * matrix2.R3.x) + (matrix1.R4.w * matrix2.R4.x),
		(matrix1.R4.x * matrix2.R1.y) + (matrix1.R4.y * matrix2.R2.y) + (matrix1.R4.z * matrix2.R3.y) + (matrix1.R4.w * matrix2.R4.y),
		(matrix1.R4.x * matrix2.R1.z) + (matrix1.R4.y * matrix2.R2.z) + (matrix1.R4.z * matrix2.R3.z) + (matrix1.R4.w * matrix2.R4.z),
		(matrix1.R4.x * matrix2.R1.w) + (matrix1.R4.y * matrix2.R2.w) + (matrix1.R4.z * matrix2.R3.w) + (matrix1.R4.w * matrix2.R4.w));
	return result;
}

#endif
