
#ifndef COLLISION_H
#define COLLISION_H

#include "cuda_runtime.h"
#include "Matrix.cuh"

typedef struct
{
    float3 colPoint;
		float3 normal;
		float t0;
		int materialId;
} HitData;

typedef struct
{
    float3 pos;
		float3 edge0;
		float3 edge1;
} Triangle;

typedef struct
{
	float3 pos;
	float3 dir;
} Ray;

typedef struct
{
	float3 pos;
	float radius;
} Sphere;

#define Epsilon 0.00001f

inline __device__ __host__ bool intersect(Ray ray, Triangle triangle, float *t0)
{
	*t0 = 0;

	float3 pVec = cross(ray.dir, triangle.edge1);

	float det = dot(triangle.edge0, pVec);
	if (det > -Epsilon && det < Epsilon)
	{
		return false;
	}

	// Inverse determinate
	float detInv = 1.0f / det;

	float3 tVec = ray.pos - triangle.pos;
	float u = dot(tVec, pVec) * detInv;
	if (u < 0.0f || u > 1.0f)
	{
		return false;
	}

	float3 qVec = cross(tVec, triangle.edge0);
	float v = dot(ray.dir, qVec) * detInv;
	if (v < 0.0f || (u + v) > 1.0f)
	{
		return false;
	}

	float t = dot(triangle.edge1, qVec) * detInv;

	*t0 = t;
	//tT0 = u;
	//tT1 = v;

	return true;
}

inline __device__ __host__ bool intersect(Ray ray, Sphere sphere,
	float3 *position, float3 *normal, float *hitT0)
{
	float3 r = ray.dir;
	float3 s = ray.pos - sphere.pos;

	float radiusSq = sphere.radius * sphere.radius;
	float rSq = lengthSquared(r);

	float ts = 0.0f;

	if (rSq < radiusSq)
	{
		// Inside of the sphere.
		ts = 0.0f;
		return false;
	}

	float sDotr = dot(s, r);
	float sSq = lengthSquared(s);
	float sigma = (sDotr * sDotr) - rSq * (sSq - radiusSq);
	if (sigma < 0.0f)
	{
		return false;
	}

	float sigmaSqrt = sqrtf(sigma);
	float lambda1 = (-sDotr - sigmaSqrt) / rSq;
	float lambda2 = (-sDotr + sigmaSqrt) / rSq;
	if (lambda1 > 1.0f || lambda2 < 0.0f)
	{
		return false;
	}

	// Intersection
	ts = fmaxf(lambda1, 0.0f);

	// Calculate the intersected position and the sphere normal.
	*position = ray.pos + (ray.dir * ts);
	*normal = normalize(*position - sphere.pos);
	*hitT0 = length(*position - ray.pos);

	return true;
}

#endif
