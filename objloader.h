
#ifndef OBJ_LOADER_H
#define OBJ_LOADER_H

#include <stdio.h>
#include <vector>

using namespace std;

// Redefine subtract, because no math is defined for cuda structs...
inline float4 operator-(float4 a, float4 b)
{
    return make_float4(a.x - b.x, a.y - b.y, a.z - b.z,  a.w - b.w);
}

inline float4 operator+(float4 a, float4 b)
{
    return make_float4(a.x + b.x, a.y + b.y, a.z + b.z,  a.w + b.w);
}

void loadObj(vector<float4> *data, char *filename, int matId, float4 translation)
{
	FILE *file = fopen(filename, "r");

	// Here we store the vertices
	vector<float4> vertices;

	char buffer[1024];
	char mode;
	float x, y, z;
	int i1, i2, i3;
	float4 tmp1, tmp2;
	while (fscanf(file, "%c", &mode) > 0)
	{
		if (mode == 'v')
		{
			// Vertex definition
			fscanf(file, "%f %f %f\n", &x, &y, &z);
			vertices.push_back(make_float4(x, y, z, 0.0f));
		}
		else if (mode == 'f')
		{
			// Face definition
			fscanf(file, "%d %d %d\n", &i1, &i2, &i3);
			// Put combined face into output data
			tmp1 = vertices[i1 - 1] + translation;
			data->push_back(tmp1);
			tmp2 = (vertices[i2 - 1] + translation) - tmp1;
			tmp2.w = matId;
			data->push_back(tmp2);
			data->push_back((vertices[i3 - 1] + translation) - tmp1);
		}
		else
		{
			// Something unknow, just skip line
			fgets(buffer, 1024, file);
		}
	}
	// Close file again
	fclose(file);
}

#endif
