/*
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cuda_math_ext.cuh"
#include "collision.cuh"
#include "Matrix.cuh"

#define PI 3.1415926536f

texture<float4, 1, cudaReadModeElementType> tri_data;

__device__ bool checkIntersect(int numTriangles, Ray ray, HitData *hitData,
	float minDist, float3 lightPos, bool checkSun)
{
	bool hit = false;
	float t0 = 0.0f;

	// Need special ray for spheres
	Ray sphereRay;
	sphereRay.pos = ray.pos;
	sphereRay.dir = ray.dir * 100.0f;

	// Iterate over all triangles
	for(int i = 0; i < numTriangles; i++)
	{
		// Fetch object data
		float4 p = tex1Dfetch(tri_data, (i * 3));
		float4 e0 = tex1Dfetch(tri_data, (i * 3) + 1);
		float4 e1 = tex1Dfetch(tri_data, (i * 3) + 2);

		// Extra data:
		// p.w: object type ID 0 = tri, 1 = sphere
		// e0.w: material ID (1 = sun)
		int objType = rintf(p.w);
		int matId = rintf(e0.w);
		if (objType == 0)
		{
			// Fetch triangle and build struct
			Triangle tri;
			tri.pos = make_float3(p.x, p.y, p.z);
			tri.edge0 = make_float3(e0.x, e0.y, e0.z);
			tri.edge1 = make_float3(e1.x, e1.y, e1.z);

			// Intersection check
			if (intersect(ray, tri, &t0))
			{
				if ((t0 < hitData->t0 ||
					hit == false) &&
					t0 >= minDist)
				{
					hit = true;
					hitData->t0 = t0;
					hitData->materialId = matId;
					hitData->colPoint = ray.pos + (ray.dir * t0);
					//TODO: invert normal if hit from backside?
					hitData->normal = normalize(cross(tri.edge0, tri.edge1));
				}
			}
		}
		else if (objType == 1)
		{
			Sphere sphere;
			sphere.pos = make_float3(p.x, p.y, p.z);
			sphere.radius = e0.x;
			float3 pos;
			float3 normal;
			if (intersect(sphereRay, sphere, &pos, &normal, &t0))
			{
				if ((t0 < hitData->t0 ||
					hit == false) &&
					t0 >= minDist)
				{
					hit = true;
					hitData->t0 = t0;
					hitData->materialId = matId;
					hitData->colPoint = pos;
					hitData->normal = normal;
				}
			}
		}
	}

	// Check sun
	if (checkSun)
	{
		Sphere sunSphere;
		sunSphere.pos = lightPos;
		sunSphere.radius = 1.00f;
		float3 pos;
		float3 normal;
		if (intersect(sphereRay, sunSphere, &pos, &normal, &t0))
		{
			if (t0 < hitData->t0 ||
				hit == false)
			{
				hit = true;
				hitData->t0 = t0;
				hitData->materialId = 1;
				hitData->colPoint = pos;
				hitData->normal = normal;
			}
		}
	}

	return hit;
}

/*
 * Paint a 2D texture with a moving red/green hatch pattern on a
 * strobing blue background.  Note that this kernel reads to and
 * writes from the texture, hence why this texture was not mapped
 * as WriteDiscard.
 */
__global__ void cuda_kernel_texture_2d(unsigned char *surface, int width, int height, size_t pitch, float t, int numTriangles,
	float3 camCenter, float3 leftUpper, float3 rightDir, float3 downDir, float3 lightPos)
{
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;
	float *pixel;

	// in the case where, due to quantization into grids, we have
	// more threads than pixels, skip the threads which don't
	// correspond to valid pixels
	if (x >= width || y >= height) return;

	// get a pointer to the pixel at (x,y)
	pixel = (float *)(surface + y*pitch) + 4*x;

	// Create Ray
	// This is where the "3D" perspective stuff will be created.
	// We have a "camera near-plane left upper position (p)",
	// a right-vector (r), down-vector (d), and also camera
	// center position (c). Note c is nearer to the screen than p.
	// p-------r
	// |
	// |   c
	// |
	// d
	// Now we just need to shoot rays from c to p. p will be moved
	// along r and d, while iterating over all pixels.
	Ray ray;
	ray.pos = camCenter;
	float3 rayDir = (leftUpper + ((x / (float)width) * rightDir) +
		((y / (float)height) * downDir)) - camCenter;
	ray.dir = normalize(rayDir);
	
	// Ambient light color
	float3 ambient = make_float3(0.3f, 0.3f, 0.3f);
	float3 materialColor = make_float3(0.8f, 0.3f, 0.3f);
	HitData hitData;
	float3 finalColor = make_float3(1.0f, 1.0f, 1.0f);

	// We had a hit -> calculate color
	bool continueRay = true;
	int iteration = 0;
	while (continueRay &&
		iteration < 10)
	{
		iteration++;
		continueRay = false;
		if (checkIntersect(numTriangles, ray, &hitData, 0.001f, lightPos, true))
		{
			// Check if sun was hit
			if (hitData.materialId == 1)
			{
				finalColor = finalColor * (make_float3(1.0f, 1.0f, 0.6f));
			}
			else if (hitData.materialId == 3)
			{
				// This is a mirror, reflect ray and do again
				continueRay = true;
				ray.pos = hitData.colPoint;
				ray.dir = normalize(reflect(ray.dir, hitData.normal));
				finalColor = finalColor * 0.8f;

				// Add specular
				float specular = pow(clamp(dot(ray.dir, reflect(normalize(lightPos - hitData.colPoint), hitData.normal)), 0.0f, 1.0f), 18.0f) * 2.0f;
				finalColor += make_float3(specular, specular, specular);
			}
			else if (hitData.materialId == 4)
			{
				// This is refraction, modify ray, and shoot again
				continueRay = true;
				ray.pos = hitData.colPoint;
				float dotRayNormal = dot(ray.dir, hitData.normal);
				float val = (dotRayNormal + 1.0f) / 4.0f;
				ray.dir = normalize(ray.dir - (hitData.normal * val));
				finalColor = finalColor * 0.8f;

				// Add specular
				float specular = pow(clamp(dot(ray.dir, reflect(normalize(lightPos - hitData.colPoint), hitData.normal)), 0.0f, 1.0f), 18.0f) * 2.0f;
				finalColor += make_float3(specular, specular, specular);
			}
			else
			{
				// Setup ambient
				float3 lightColor;

				// Setup material color
				switch (hitData.materialId)
				{
					case 0: materialColor = make_float3(0.8f, 0.3f, 0.3f); break;
					case 2: materialColor = make_float3(0.3f, 0.8f, 0.3f); break;
					// Pseudo texture
					case 5:
						// Calc U/V coordinates
						if ((((int)floor(hitData.colPoint.x + 0.5f) +
							(int)floor(hitData.colPoint.z + 0.5f) + 1000) % 2) == 0)
						{
							materialColor = make_float3(0.5f, 0.3f, 0.8f);
						}
						else
						{
							materialColor = make_float3(0.1f, 0.1f, 0.1f);
						}
						break;
				}

				// Check if light(sun) is visible
				Ray lightRay;
				lightRay.pos = hitData.colPoint;
				float3 lightRayDir = lightPos - hitData.colPoint;
				lightRay.dir = normalize(lightRayDir);
				// Calculate "would be light color"
				float distSquared = lengthSquared(lightPos - hitData.colPoint) / 50.0f;
				float light = clamp(dot(lightRay.dir, hitData.normal), 0.0f, 1.0f) / distSquared;
				HitData lightHitData;
				float specular = pow(clamp(dot(ray.dir, reflect(lightRay.dir, hitData.normal)), 0.0f, 1.0f), 18.0f);
				if (checkIntersect(numTriangles, lightRay, &lightHitData, 0.001f, lightPos, false))
				{
					// Light is NOT visible, tone down light
					light *= clamp(lightHitData.t0 / 3.0f, 0.0f, 0.9f);
				}
				else
				{
					light = light + specular * 2.0f;
				}
				lightColor = make_float3(light, light, light);
				finalColor = finalColor * (materialColor * (lightColor + ambient));
			}
		}
		else
		{
			// No hit, background color
			finalColor = finalColor * (make_float3(0.6f, 0.6f, 1.0f));
		}
	}

	// Set pixel values
	pixel[0] = finalColor.x; // red
	pixel[1] = finalColor.y; // green
	pixel[2] = finalColor.z; // blue
	pixel[3] = 1.0f; // alpha
}

extern "C"
{
	void cuda_texture_2d(void *surface, int width, int height, size_t pitch, float t, int numTriangles)
	{
			cudaError_t error = cudaSuccess;

			// Setup camera (Needs to be improved)
			float3 camTarget = make_float3(0.0f, 0.0f, 0.0f);
			float3 camUp = make_float3(0.0f, 1.0f, 0.0f);
			float3 camCenter = make_float3(0.0f, -1.0f, -7.0f);
			if (t > 2 * PI)
			{
				camCenter =
					make_float3(sinf((t - (2 * PI)) / 5.0f) * 5.0f,
					-1.0f,
					cos((t - (2 * PI)) / 5.0f) * -7.0f);
			}

			float3 camDir = normalize(camTarget - camCenter);
			float3 camLeft = normalize(cross(camDir, camUp));
			float planeDist = 5.0f;
			float fov = 3.0f;
			float3 realCamUp = normalize(cross(camLeft, camDir));
			float3 frontCenter = camCenter + (camDir * planeDist);
			float3 leftUpper = frontCenter + (camLeft * fov) + (camUp * fov);
			float3 rightDir = -camLeft * (2.0f * fov);
			float3 downDir = -realCamUp * (2.0f * fov);

			// Light setup
			float3 lightPos = make_float3(-5.0f + cosf(t) * 5.0f, -4.0f, -5.0f);
				//make_float3(-5.0f, -4.0f, -5.0f);

			dim3 Db = dim3(16, 16);   // block dimensions are fixed to be 256 threads
			dim3 Dg = dim3((width+Db.x-1)/Db.x, (height+Db.y-1)/Db.y);

			cuda_kernel_texture_2d<<<Dg,Db>>>((unsigned char *)surface, width, height, pitch, t, numTriangles,
				camCenter, leftUpper, rightDir, downDir, lightPos);

			error = cudaGetLastError();

			if (error != cudaSuccess)
			{
					printf("cuda_kernel_texture_2d() failed to launch error = %d\n", error);
			}
	}

	void bindTriangles(float *dev_triangle_p, unsigned int number_of_triangles)
	{
		tri_data.normalized = false;                      // access with normalized texture coordinates
		tri_data.filterMode = cudaFilterModePoint;        // Point mode, so no 
		tri_data.addressMode[0] = cudaAddressModeWrap;    // wrap texture coordinates

		size_t size = sizeof(float4) * number_of_triangles * 3;       
		cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float4>();
		cudaBindTexture(0, tri_data, dev_triangle_p, channelDesc, size);
	}
}
